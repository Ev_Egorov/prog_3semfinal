#include "Graph.h"
#include <fstream>
#include <sstream>

void graph_methods::Floyd_Warshall(vector<vector<int>> &map, int countVert)
{
    for (int k = 0; k < countVert; ++k)
        for (int i = 0; i < countVert; ++i)
            for (int j = 0; j < countVert; ++j)
                if (map[i][j])
                    map[i][j] = std::min(map[i][j], map[k][j] + map[i][k]);
}

void graph_methods::FillGraph(vector<vector<int>> &map, int countVert, string &input) {
    std::stringstream test_stream;
    test_stream << input;

    int tmp;
    for (int i = 0; i < countVert; ++i) {
        map[i].reserve(countVert);
        for (int j = 0; j < countVert; ++j) {
            test_stream >> tmp;
            map[i].push_back(tmp);
        }
    }
}
