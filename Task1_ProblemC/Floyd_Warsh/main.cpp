#include "Graph.h"
#include <iostream>
#include <algorithm>
#include <fstream>

using std::cin;
using std::cout;
using std::vector;

#ifndef GTEST_TEST

int main()
{
    int countVert;
    cin >> countVert;
    vector<vector<int> > map (countVert);

    for (int i = 0; i < countVert; ++i) {
        map[i].resize(countVert);
        for (int j = 0; j < countVert; ++j) {
            cin >> map[i][j];
        }
    }

    graph_methods action;

    action.Floyd_Warshall(map, countVert);

    for (int i = 0; i < countVert; ++i) {
        for (int j=0; j < countVert; ++j) {
            cout << map[i][j] << " ";
        }
        cout << "\n";
    }

    return 0;
}

#endif