//
// Created by evgeniy on 15.12.16.
//
#include <gtest/gtest.h>
#include "../Graph.h"
#include <iostream>

class GraphMaintain : public testing::Test {
public:
    graph_methods action;
};



TEST_F(GraphMaintain, test_algo) {
    int vertex_count = 4;
    vector<vector<int>> graph(vertex_count);
    std::string input = "0 5 9 100 100 0 2 8 100 100 0 7 4 100 100 0";

    action.FillGraph(graph, vertex_count, input);
    action.Floyd_Warshall(graph, vertex_count);

    vector<vector<int>> ans_graph(vertex_count);
    std::string answer = "0 5 7 13 12 0 2 8 11 16 0 7 4 9 11 0";
    action.FillGraph(ans_graph, vertex_count, answer);

    for (int i =0; i < vertex_count; ++i) {
        for (int j = 0; j < vertex_count; ++j)
            std::cout << graph[i][j];
        std::cout << '\n';
    }

    EXPECT_EQ(graph, ans_graph);
}
