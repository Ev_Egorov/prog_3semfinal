#pragma once

#include <vector>
#include <string>

using std::string;
using std::vector;


class graph_methods {
public:
    void Floyd_Warshall(vector<vector<int>>&, int);
    void FillGraph(vector<vector<int>>&, int, string&);
};